import { Switch, Route } from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import MainPage from "./pages/MainPage";
import TourPage from './pages/TourPage'
import SignUpPage from "./pages/SignUpPage";
import HomePage from "./pages/HomePage";
import PanelPage from "./pages/PanelPage";
import ProfilePage from "./pages/ProfilePage";
import CartPage from "./pages/CartPage";


const Routes = ({ isLoggedIn }) => {
  return (
    <>
      <Switch>
        <Route exact path="/" component={MainPage} />
        <Route exact path="/home" component={HomePage} />
        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/signup" component={SignUpPage} />
        <Route exact path='/tours/:id' component={TourPage} />
        <Route exact path="/panel/home" component={PanelPage} />
        <Route exact path="/panel/profile" component={ProfilePage} />
        <Route exact path="/panel/cart" component={CartPage} />
      </Switch>
    </>
  );
};

export default Routes;
