import { ReactComponent as Logo } from "../assets/Logo.svg";
import { ReactComponent as Photo } from "../assets/LoginPhoto.svg";
import { makeStyles } from "@material-ui/styles";
import { Paper } from "@material-ui/core";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
  logo: {
    position: "absolute",
    top: "60px",
    left: "100px",
    zIndex: "5",
    cursor: "pointer",
  },
  paper: {
    height: "100vh",
    marginLeft: "50px",
    marginRight: "50px",
  },
  photo: {
    position: "absolute",
    top: "15px",
    left: "-20px",
    zIndex: "4",
    height: "98vh",
    bottom: "15px",
  },
});
const SignLayout = ({ children }) => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <>
      <Paper className={classes.paper}>
        <Logo className={classes.logo} onClick={() => history.push("/")} />
        <Photo className={classes.photo} />
        {children}
      </Paper>
    </>
  );
};

export default SignLayout;
