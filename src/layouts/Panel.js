import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Logo from "../components/logo/WhiteLogo";
import { ReactComponent as CartIcon } from "../assets/CartIcon.svg";
import { ReactComponent as HomeIcon } from "../assets/HomeIcon.svg";
import { ReactComponent as PersonIcon } from "../assets/ProfileIcon.svg";
import { useHistory, useLocation } from "react-router-dom";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },

  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#2F49D1",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  background: {
    backgroundColor: "#2F49D1",
    color: "white",
  },
  selectedLi: {
    backgroundColor: "rgb(82,104,216)",
  },
}));

const Panel = (props) => {
  const history = useHistory();
  const location = useLocation();
  const { window, children } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div className={classes.background}>
      <br></br>
      <Logo />
      <Divider />
      <List>
        {/* <ListItem
          button
          key={"Home"}
          onClick={() => history.push("/panel/home")}
          className={
            location.pathname === "/panel/home" ? classes.selectedLi : undefined
          }
        >
          <ListItemIcon>{<HomeIcon height="25px" width="25px" />}</ListItemIcon>
          <ListItemText primary={"Home"} />
        </ListItem> */}
        <ListItem
          button
          key={"Cart"}
          onClick={() => history.push("/panel/cart")}
          className={
            location.pathname === "/panel/cart" ? classes.selectedLi : undefined
          }
        >
          <ListItemIcon>{<CartIcon height="25px" width="25px" />}</ListItemIcon>
          <ListItemText primary={"Cart"} />
        </ListItem>
        <ListItem
          button
          key={"Profile"}
          onClick={() => history.push("/panel/profile")}
          className={
            location.pathname === "/panel/profile"
              ? classes.selectedLi
              : undefined
          }
        >
          <ListItemIcon>
            {<PersonIcon height="25px" width="25px" />}
          </ListItemIcon>
          <ListItemText
            primary={"Profile"}
            className={
              location.pathname === "/panel/profile"
                ? classes.selectedLi
                : undefined
            }
          />
        </ListItem>
      </List>
      <Divider />
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <nav className={classes.drawer} aria-label="mailbox folders">
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>{children}</main>
    </div>
  );
};

export default Panel;
