import { GET_TOURS_REQUEST, GET_TOURS_SUCCESS } from "./toursActionTypes";

const initialState = {
  tours: [],
  loading: false,
};

export const toursReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TOURS_REQUEST:
      return { ...state, tours: [], loading: true };
    case GET_TOURS_SUCCESS:
      return { ...state, tours: action.payload, loading: false };

    default:
      return state;
  }
};
