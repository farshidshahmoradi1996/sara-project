import { mock__getTours } from "../../assets/mock/toures__mock";
import { GET_TOURS_REQUEST, GET_TOURS_SUCCESS } from "./toursActionTypes";

export const getTours = ({
  start = null,
  destination = null,
  maxPrice = null,
  minPrice = null,
}) => {
  return async (dispatch) => {
    dispatch({ type: GET_TOURS_REQUEST });
    try {
      const filters = {
        start,
        destination,
        maxPrice,
        minPrice,
      };
      console.log(filters);
      const result = await mock__getTours(filters);
      dispatch({ type: GET_TOURS_SUCCESS, payload: result });
    } catch (e) {}
  };
};
