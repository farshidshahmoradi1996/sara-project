import { combineReducers } from "redux";
import { cartReducer } from "./cart/cartReducer";
import userReducer from "./user/userreducer";
import { toursReducer } from "./tours/toursReducer";
import { filterReducer } from "./filter/filterReducer";

const rootReducer = combineReducers({
  users: userReducer,
  cart: cartReducer,
  tours: toursReducer,
  filters: filterReducer,
});

export default rootReducer;
