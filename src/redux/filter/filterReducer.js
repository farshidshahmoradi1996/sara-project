import { SET_FILTER_ITEM, SET_FILTERS } from "./filterActionTypes";

const initialState = {
  start: null,
  destination: null,
  minPrice: null,
  maxPrice: null,
};

export const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_FILTER_ITEM:
      return { ...state, [action.payload.item]: action.payload.value };
    case SET_FILTERS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
