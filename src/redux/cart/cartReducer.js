import {
  ADD_ITEM_TO_CART,
  REMOVE_ITEM_FROM_CART,
  SET_CART,
} from "./cartActionTypes";

const initialState = {
  items: localStorage.getItem("cart")
    ? JSON.parse(localStorage.getItem("cart"))
    : [],
};

export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM_TO_CART:
      return { ...state, items: [...state.items, action.payload] };
    case REMOVE_ITEM_FROM_CART:
      return {
        ...state,
        items: state.items?.filter((_i) => _i.id !== action.payload),
      };
    case SET_CART:
      return {
        ...state,
        items: action.payload,
      };

    default:
      return state;
  }
};
