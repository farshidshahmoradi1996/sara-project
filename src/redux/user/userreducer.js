import {
  USER_LOGIN_FAILED,
  USER_LOGIN_LOADING,
  USER_LOGIN_SUCCESS,
  USER_SIGN_UP_FAILED,
  USER_SIGN_UP_SUCCESS,
  USER_SIGN_UP_LOADING,
  SET_USER,
  UPDATE_CURRENT_USER,
} from "./userActionTypes";
import { v4 as uuidv4 } from "uuid";
const default_users = [
  {
    id: uuidv4(),
    email: "test@gmail.com",
    password: "123456789",
    first_name: "Test Account",
    last_name: "Test Account",
    phone: "9120000000",
    id_Card: "BBN-595554",
    passport_no: "2954-5454-445-5454-IR",
    address: "No 12,Shahid Jabari Street , Gandy , Tehran Province , Iran",
    post_address: "3154965-659184",
  },
];
const initialState = {
  user: {
    data: localStorage.getItem("session")
      ? JSON.parse(localStorage.getItem("session"))
      : null,
    loading: false,
    error: false,
  },
  user_sign_up_loading: false,
  all_users: localStorage.getItem("all_users")
    ? JSON.parse(localStorage.getItem("all_users"))
    : default_users,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        user: {
          data: action.payload,
          loading: false,
          error: false,
        },
      };
    case USER_LOGIN_FAILED:
      return {
        ...state,
        user: {
          ...state.user,
          loading: false,
          error: true,
        },
      };
    case USER_LOGIN_LOADING:
      return {
        ...state,
        user: {
          ...state.user,
          loading: true,
        },
      };
    case USER_SIGN_UP_LOADING:
      return {
        ...state,
        user_sign_up_loading: true,
      };
    case USER_SIGN_UP_SUCCESS:
      return {
        ...state,
        all_users: [...state.all_users, action.payload],
        user_sign_up_loading: false,
      };
    case USER_SIGN_UP_FAILED:
      return {
        ...state,
        user_sign_up_loading: false,
      };
    case SET_USER:
      return {
        ...state,
        user: {
          data: action.payload,
          loading: false,
          error: false,
        },
      };
    case UPDATE_CURRENT_USER:
      const all_usersClone = JSON.parse(JSON.stringify(state.all_users));
      const index = all_usersClone.findIndex(
        (_i) => _i.id === action.payload.id
      );
      if (index > -1) {
        all_usersClone[index] = action.payload;
      }
      return {
        ...state,
        user: {
          data: { ...state.user.data, ...action.payload },
          loading: false,
          error: false,
        },
        all_users: all_usersClone,
      };
    default:
      return state;
  }
};

export default userReducer;
