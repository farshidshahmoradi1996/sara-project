import {
  USER_LOGIN_FAILED,
  USER_LOGIN_LOADING,
  USER_LOGIN_SUCCESS,
  USER_SIGN_UP_FAILED,
  USER_SIGN_UP_LOADING,
  USER_SIGN_UP_SUCCESS,
} from "./userActionTypes";
import { mock__login, mock__signup } from "../../assets/mock/login__mock";
import { toast } from "react-toastify";
import { v4 as uuidv4 } from "uuid";
import store from "./../store";
export const userLoginSuccess = (payload) => ({
  type: USER_LOGIN_SUCCESS,
  payload,
});

export const userLoginLoading = () => ({
  type: USER_LOGIN_LOADING,
});

export const userLoginFailed = () => ({
  type: USER_LOGIN_FAILED,
});
export const userSignUpLoading = () => ({
  type: USER_SIGN_UP_LOADING,
});

export const userSignUpFailed = () => ({
  type: USER_SIGN_UP_FAILED,
});
export const userSignUpSuccess = (payload) => ({
  type: USER_SIGN_UP_SUCCESS,
  payload,
});

export const userLogin = ({ email, password }) => {
  return async (dispatch) => {
    dispatch(userLoginLoading());

    try {
      await mock__login({ email, password });

      const {
        users: { all_users },
      } = store.getState();

      const findUser = all_users.find(
        (_i) => _i.email.trim().toLowerCase() === email.trim().toLowerCase()
      );
      if (!findUser) {
        throw new Error(`no user found with ${email}. please first sign up .`);
      }
      if (findUser?.password === password) {
        dispatch(userLoginSuccess(findUser));
        toast.success(`Welcome ${findUser.first_name} ${findUser.last_name}`);
        return;
      }
      throw new Error(`incorrect password .`);
    } catch (e) {
      dispatch(userLoginFailed());
      console.log(e);
      toast.error(e.message);
    }
  };
};

export const userSignUp = ({ email, password }) => {
  return async (dispatch) => {
    dispatch(userSignUpLoading());
    try {
      await mock__signup();
      dispatch(
        userSignUpSuccess({
          id: uuidv4(),
          email: email.toLowerCase(),
          password,
          first_name: "",
          last_name: "",
          phone: "",
          id_Card: "",
          passport_no: "",
          address: "",
          post_address: "",
        })
      );
      toast.success(
        `${email} register was successfully.please login to your account. `
      );
    } catch (e) {
      dispatch(userSignUpFailed());
      toast.error(e.message);
    }
  };
};
