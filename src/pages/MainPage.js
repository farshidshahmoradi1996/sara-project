import LandingPhoto from "../components/landing/LandingPhoto";
import FilterFrom from "../components/filter/FilterForm";
import Description from "../components/landing/Description";
import { Button, Grid, Typography } from "@material-ui/core";
import Header from "../components/header/header";
import Footer from "../components/footer/Footer";
import CardDirectory from "../components/CardDirectory/CardDirectory";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

const MainPage = () => {
  const history = useHistory();
  const isLoggedIn = useSelector((state) => state.users?.user?.data);
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <section>
            <LandingPhoto />
          </section>
          <Grid item xs={12}>
            <Header isLoggedIn={!!isLoggedIn} />
          </Grid>
          <Grid item xs={12}>
            <FilterFrom />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Description />
        </Grid>
        <Grid item xs={12}>
          <Typography
            component="h1"
            variant="h3"
            align="center"
            color="primary"
          >
            Tour Packages
          </Typography>
        </Grid>
        <Grid item xs={11}></Grid>
        <Grid item xs={1}>
          <Button
            onClick={() => history.push("/home")}
            endIcon={<ArrowForwardIosIcon />}
          >
            More
          </Button>
        </Grid>
        <Grid item xs={12}>
          <br></br>
          <CardDirectory />
        </Grid>
        <Grid item xs={12}>
          <Footer />
        </Grid>
      </Grid>
    </>
  );
};

export default MainPage;
