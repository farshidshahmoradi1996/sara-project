import { Paper, Typography, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FilterFrom from "../components/filter/Filter";
import PriceFilter from "../components/filter/PriceFilter";
import Header from "../components/header/header";
import TicketDirectory from "../components/ticketdirectroy/TicketDirectory";
import CircularProgress from "@material-ui/core/CircularProgress";
import { getTours } from "../redux/tours/toursActions";

const useStyles = makeStyles({
  bar: {
    padding: "15px",
    width: "430px",
    marginTop: "120px",
    height: "600px",
  },
  directory: {
    backgroundColor: "#e0e0eb",
    padding: "15px",
    width: "970px",
    marginTop: "120px",
  },
});

const HomePage = () => {
  const classes = useStyles();
  const isLoggedIn = useSelector((state) => state.users?.user?.data);
  const loading = useSelector((state) => state.tours.loading);
  const dispatch = useDispatch();
  const tours = useSelector((state) => state.tours.tours);

  useEffect(() => {
    if (tours?.length === 0 && !loading) dispatch(getTours({}));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Paper>
        <Header isLoggedIn={isLoggedIn} />
      </Paper>
      <FilterFrom />
      {loading ? (
        <div
          style={{
            marginTop: 200,
            width: "100vw",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <CircularProgress />
        </div>
      ) : (
        <Grid container style={{ padding: 24 }}>
          <Grid item xs={4}>
            <Paper className={classes.bar}>
              <Typography color="primary">Other Filters</Typography>
              <PriceFilter />
            </Paper>
          </Grid>
          <Grid item xs={8}>
            <Paper className={classes.directory}>
              <Typography color="primary">Results:</Typography>
              <br></br>
              <TicketDirectory />
            </Paper>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default HomePage;
