import Layout from "../layouts/Panel";
import { Button, Divider, Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import Ticket from "../components/ticket/TicketComponent";
import { toMoneyFormat } from "../utils/common";
import { toast } from "react-toastify";
import { SET_CART } from "../redux/cart/cartActionTypes";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
  directory: {
    backgroundColor: "#e0e0eb",
    padding: "15px",
    width: "1250px",
    marginTop: "5px",
  },
});

const CartPage = () => {
  const classes = useStyles();
  const carts = useSelector((state) => state.cart.items);
  const history = useHistory();
  const dispatch = useDispatch();
  const price = carts?.length
    ? carts
        ?.map((_i) => _i.prices)
        ?.map((_i) =>
          _i?.reduce((prev, curr) => {
            return prev.price < curr.price ? prev : curr;
          })
        )
        ?.map((_i) => _i.price)
        ?.reduce((prev, curr) => {
          return prev + curr;
        })
    : 0;

  const checkout = () => {
    toast.success(`Checkout Complete You Should Pay ${toMoneyFormat(price)} T`);
    //delete All from Cards
    dispatch({ type: SET_CART, payload: [] });
  };
  return (
    <>
      <Layout>
        <Paper className={classes.directory}>
          <Typography color="primary">Cart:</Typography>
          {carts?.map((_i) => (
            <Ticket info={_i} />
          ))}
          {carts?.length === 0 && (
            <div>
              <p>Your Cart Is Empty </p>{" "}
              <Button onClick={() => history.push("/home")} color="secondary">
                Back
              </Button>
            </div>
          )}

          <br></br>
          <Divider />
          <Typography color="primary">
            Total Price: {toMoneyFormat(price)}
          </Typography>
          <Button
            disabled={carts?.length === 0}
            onClick={checkout}
            color="primary"
            variant="contained"
          >
            Check Out
          </Button>
        </Paper>
      </Layout>
    </>
  );
};

export default CartPage;
