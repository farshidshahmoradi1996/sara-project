import Layout from "../layouts/Panel";
import CardDirectory from "../components/CardDirectory/CardDirectory";
import { Typography } from "@material-ui/core";
const PanelPage = () => {
  return (
    <>
      <Layout>
        <Typography component="h1" variant="h4" color="primary">
          Reserved Travels:
        </Typography>
        <br></br>
        <CardDirectory />
        <br></br>
        <Typography component="h1" variant="h4" color="primary">
          You've Traveled:
        </Typography>
        <br></br>
        <CardDirectory />
      </Layout>
    </>
  );
};

export default PanelPage;
