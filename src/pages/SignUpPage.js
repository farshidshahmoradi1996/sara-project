import { ReactComponent as Logo } from "../assets/Logo.svg";
import { ReactComponent as Photo } from "../assets/SignUpPhoto.svg";
import { makeStyles } from "@material-ui/styles";
import { Paper, Grid } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import SignUpForm from "../components/forms/SignUnForm";
import { useEffect } from "react";
import { useSelector } from "react-redux";

const useStyles = makeStyles({
  logo: {
    position: "absolute",
    top: "60px",
    left: "100px",
    zIndex: "5",
    cursor: "pointer",
  },
  paper: {
    height: "100vh",
    marginLeft: "50px",
    marginRight: "50px",
  },
  photo: {
    position: "absolute",
    top: "15px",
    left: "-20px",
    zIndex: "4",
    height: "98vh",
    bottom: "15px",
  },
});
const LoginPage = ({ children }) => {
  const classes = useStyles();
  const history = useHistory();
  const isLoggedIn = useSelector((state) => state.users?.user?.data);
  useEffect(() => {
    if (isLoggedIn) history.push("/");
  }, [history, isLoggedIn]);

  return (
    <>
      <Paper className={classes.paper}>
        <Grid container>
          <Grid item xs={6}>
            <Logo className={classes.logo} onClick={() => history.push("/")} />
            <Photo className={classes.photo} />
          </Grid>
          <Grid item xs={6}>
            <SignUpForm />
          </Grid>
        </Grid>
        {children}
      </Paper>
    </>
  );
};

export default LoginPage;
