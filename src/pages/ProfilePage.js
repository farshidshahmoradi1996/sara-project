import Layout from "../layouts/Panel";
import ProfileWidget from "../components/profile/ProfileWidget";
import ProfileTable from "../components/profile/ProfileTable";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
const ProfilePage = () => {
  const history = useHistory();
  const isLoggedIn = useSelector((state) => state.users?.user?.data);
  useEffect(() => {
    if (!isLoggedIn) history.push("/");
  }, [history, isLoggedIn]);
  return (
    <>
      <Layout>
        <ProfileWidget />
        <br></br>
        <Divider />
        <br></br>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <ProfileTable />
          </Grid>
        </Grid>
      </Layout>
    </>
  );
};

export default ProfilePage;
