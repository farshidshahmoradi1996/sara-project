import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  landingAccomodation: {
    display: "flex",
    backgroundColor: "#7D92FF",
    width: "100%",
    height: "500px",
    alignItems: "center",
    padding: "20px",
  },
});
