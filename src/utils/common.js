export function toMoneyFormat(value) {
  if (!value) return value;
  if (value.length < 3) return value.toString();
  return value
    .toString()
    .replace(/,/g, "")
    .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1٬");
}
