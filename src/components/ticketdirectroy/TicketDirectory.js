import { Button, Grid } from "@material-ui/core";
import { useSelector } from "react-redux";

import Ticket from "../ticket/TicketComponent";

const Directory = (props) => {
  const tours = useSelector((state) => state.tours.tours);
  return (
    <>
      <Grid container spacing={4}>
        {tours.map((item) => (
          <Grid item xs={12} key={item.id}>
            <Ticket info={item} />
          </Grid>
        ))}
      </Grid>
      {tours?.length === 0 && (
        <div style={{ marginTop: "50" }}>
          <p style={{ textAlign: "center" }}>Sorry No Tour Found ,</p>
        </div>
      )}
    </>
  );
};
export default Directory;
