import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  paper: {
    padding: "20px",
  },
  listItem: {
    margin: "10px",
  },
  edit: {
    cursor: "pointer",
  },
  modalPaper: {
    padding: "20px",
    margin: "100px",
  },
});
