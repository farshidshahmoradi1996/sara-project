import { Grid, Typography } from "@material-ui/core";
import { ReactComponent as EditIcon } from "../../assets/EditIcon.svg";
import { ReactComponent as Avatar } from "../../assets/Avatar.svg";
import { ReactComponent as StarIcon } from "../../assets/StarIcon.svg";
import { useSelector } from "react-redux";

const ProfileWidget = () => {
  const user = useSelector((state) => state.users.user.data);
  return (
    <>
      <Grid container spacing={5}>
        <Grid item xs={3}>
          <Avatar />
        </Grid>
        <Grid item xs={4}>
          <br></br>
          <Typography component="h1" variant="h3" color="primary">
            {user?.first_name} {user?.last_name}
          </Typography>
          <Typography component="h4" variant="h6" color="primary">
            <br></br>
            Points: 123
          </Typography>
          <StarIcon height="20px" width="20px" />
        </Grid>
        <Grid item xs={1}>
          <EditIcon height="25px" width="25px" />
        </Grid>
      </Grid>
    </>
  );
};

export default ProfileWidget;
