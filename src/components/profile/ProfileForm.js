import { TextField, Grid, Paper, Button, Typography } from "@material-ui/core";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { UPDATE_CURRENT_USER } from "../../redux/user/userActionTypes";
import { useStyles } from "./ProfileStyles";
const InfoForm = ({ info, onClose }) => {
  const classes = useStyles();

  const [userData, setUserData] = useState(info);
  const dispatch = useDispatch();
  const updateUser = () => {
    dispatch({ type: UPDATE_CURRENT_USER, payload: userData });
  };

  return (
    <>
      <Paper className={classes.modalPaper}>
        <Typography color="primary">Personal Information</Typography>
        <form>
          <Grid container spacing={2}>
            <Grid item xs={6} spacing={2}>
              <TextField
                className={classes.listItem}
                label="Name"
                fullWidth
                value={userData?.first_name}
                onChange={(event) =>
                  setUserData((prev) => ({
                    ...prev,
                    first_name: event.target.value,
                  }))
                }
              />
            </Grid>
            <Grid item xs={6} spacing={2}>
              <TextField
                className={classes.listItem}
                label="Last Name"
                fullWidth
                value={userData?.last_name}
                onChange={(event) =>
                  setUserData((prev) => ({
                    ...prev,
                    last_name: event.target.value,
                  }))
                }
              />
            </Grid>
            <Grid item xs={6} spacing={2}>
              <TextField
                className={classes.listItem}
                label="Phone"
                fullWidth
                value={userData?.phone}
                onChange={(event) =>
                  setUserData((prev) => ({
                    ...prev,
                    phone: event.target.value,
                  }))
                }
              />
            </Grid>
            <Grid item xs={6} spacing={2}>
              <TextField
                className={classes.listItem}
                label="Post Address"
                fullWidth
                value={userData?.post_address}
                onChange={(event) =>
                  setUserData((prev) => ({
                    ...prev,
                    post_address: event.target.value,
                  }))
                }
              />
            </Grid>
            <Grid item xs={12} spacing={2}>
              <TextField
                className={classes.listItem}
                label="Id Card"
                fullWidth
                value={userData?.id_Card}
                onChange={(event) =>
                  setUserData((prev) => ({
                    ...prev,
                    id_Card: event.target.value,
                  }))
                }
              />
            </Grid>
            <Grid item xs={12} spacing={2}>
              <TextField
                className={classes.listItem}
                label="Passport No"
                fullWidth
                value={userData?.passport_no}
                onChange={(event) =>
                  setUserData((prev) => ({
                    ...prev,
                    passport_no: event.target.value,
                  }))
                }
              />
            </Grid>
            <Grid item xs={12} spacing={2}>
              <TextField
                className={classes.listItem}
                label="Address"
                fullWidth
                value={userData?.address}
                onChange={(event) =>
                  setUserData((prev) => ({
                    ...prev,
                    address: event.target.value,
                  }))
                }
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                fullWidth
                color="primary"
                onClick={() => {
                  onClose();
                  updateUser();
                }}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </>
  );
};

export default InfoForm;
