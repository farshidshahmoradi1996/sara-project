import { useState } from "react";
import { Paper, Grid, Typography, Modal, Button } from "@material-ui/core";
import { ReactComponent as EditIcon } from "../../assets/EditIcon.svg";
import { useStyles } from "./ProfileStyles";
import Form from "./ProfileForm";
import { useDispatch, useSelector } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { SET_USER } from "../../redux/user/userActionTypes";
import { toast } from "react-toastify";
import { SET_CART } from "../../redux/cart/cartActionTypes";
const ProfileTable = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [openLogoutConfirm, setOpenLogoutConfirm] = useState(false);
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const user = useSelector((state) => state.users.user.data);
  const logout = () => {
    setOpenLogoutConfirm(false);
    dispatch({ type: SET_USER, payload: null });

    dispatch({ type: SET_CART, payload: [] });
    toast.success("you logout successfully.");
    localStorage.removeItem("session");
  };
  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Form
          info={user}
          onClose={() => {
            handleClose();
            toast.success("your profile was updated successfully.");
          }}
        />
      </Modal>
      <Dialog
        open={openLogoutConfirm}
        onClose={() => setOpenLogoutConfirm(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Logout!"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are You Sure That You Want To Logout From Your Profile ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpenLogoutConfirm(false)} color="primary">
            Cancel
          </Button>
          <Button color={"secondary"} onClick={logout} autoFocus>
            Yes,Logout
          </Button>
        </DialogActions>
      </Dialog>
      <Paper className={classes.paper}>
        <Grid container spacing={2}>
          <Grid item xs={11} spacing={2}>
            <Typography color="primary">Personal Information</Typography>
          </Grid>
          <Grid item xs={1} spacing={2}>
            <EditIcon
              height="20px"
              width="20px"
              className={classes.edit}
              onClick={handleOpen}
            />
          </Grid>
          <Grid item xs={6} spacing={2}>
            <Typography className={classes.listItem}>
              Name: {user?.first_name}
            </Typography>
          </Grid>
          <Grid item xs={6} spacing={2}>
            <Typography className={classes.listItem}>
              Last Name: {user?.last_name}
            </Typography>
          </Grid>
          <Grid item xs={6} spacing={2}>
            <Typography className={classes.listItem}>
              Phone: {user?.phone}
            </Typography>
          </Grid>
          <Grid item xs={6} spacing={2}>
            <Typography className={classes.listItem}>
              Post Add: {user?.postAdd}
            </Typography>
          </Grid>
          <Grid item xs={12} spacing={2}>
            <Typography className={classes.listItem}>
              Id Card: {user?.id_Card}
            </Typography>
          </Grid>
          <Grid item xs={12} spacing={2}>
            <Typography className={classes.listItem}>
              Passport No: {user?.passport_no}
            </Typography>
          </Grid>
          <Grid item xs={12} spacing={2}>
            <Typography className={classes.listItem}>
              Address: {user?.address}
            </Typography>
          </Grid>
        </Grid>
      </Paper>

      <br />
      <Button onClick={() => setOpenLogoutConfirm(true)} color={"secondary"}>
        Logout
      </Button>
    </>
  );
};

export default ProfileTable;
