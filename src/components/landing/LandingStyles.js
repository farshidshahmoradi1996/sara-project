import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  photo: {
    position: "relative",
    width: "50%",
    marginLeft: "50%",
    height: "100%",
    top: "-62px",
    zIndex: "-1",
  },
  description: {
    width: "60%",
    height: "300px",
    marginRight: "20%",
    marginLeft: "20%",
    marginBottom: "50px",
  },
});
