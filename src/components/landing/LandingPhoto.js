import { ReactComponent as Photo } from "../../assets/LandingPhoto.svg";
import { useStyles } from "./LandingStyles";
const LandingPhoto = () => {
  const classes = useStyles();
  return (
    <>
      <Photo className={classes.photo} />
    </>
  );
};

export default LandingPhoto;
