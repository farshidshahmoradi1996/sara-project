import { ReactComponent as DescriptionPicture } from "../../assets/Description.svg";
import { useStyles } from "./LandingStyles";
const Description = () => {
  const classes = useStyles();
  return (
    <>
      <DescriptionPicture className={classes.description} />
    </>
  );
};

export default Description;
