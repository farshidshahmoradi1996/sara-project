import { Grid } from "@material-ui/core";
import { TOURS } from "../../assets/mock/toures__mock";
import Card from "../card/Card";

const CardDirectory = () => {
  return (
    <>
      <Grid container spacing={3} alignItems="center" justify="space-around">
        {TOURS.slice(0, 4).map((item) => (
          <Grid item xs={3}>
            <Card info={item} key={item.id} />
          </Grid>
        ))}
      </Grid>
    </>
  );
};

export default CardDirectory;
