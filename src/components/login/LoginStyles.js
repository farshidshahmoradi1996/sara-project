import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  photo: {
    position: "absolute",
    zIndex: "-1",
    height: "100vh",
    top: "0",
    left: "0",
  },
});
