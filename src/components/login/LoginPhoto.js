import { ReactComponent as Photo } from "../../assets/LoginPhoto.svg";
import { useStyles } from "./LoginStyles";

const LoginPhoto = () => {
  const classes = useStyles();
  return (
    <>
      <Photo className={classes.photo} />
    </>
  );
};

export default LoginPhoto;
