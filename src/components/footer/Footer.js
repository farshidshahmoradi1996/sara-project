import { ReactComponent as FooterPhoto } from "../../assets/Footer.svg";
import { useStyles } from "./FooterStyles";

const Footer = () => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.root}>
        <FooterPhoto className={classes.photo} />
      </div>
    </>
  );
};

export default Footer;
