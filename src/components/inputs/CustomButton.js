import { Button, Icon } from "@material-ui/core";
import { useHistory } from "react-router-dom";

const CustomButton = ({ icon, name, address }) => {
  const history = useHistory();
  return (
    <>
      <Button onClick={() => history.push(`/${address}`)}>
        <Icon>{icon}</Icon>
        {name}
      </Button>
    </>
  );
};

export default CustomButton;
