import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  logo: {
    width: "75%",
    marginLeft: "15%",
    cursor: "pointer",
    top: "0",
  },
});
