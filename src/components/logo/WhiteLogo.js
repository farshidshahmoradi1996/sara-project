import { ReactComponent as LogoSVG } from "../../assets/WhiteLogo.svg";
import { useHistory } from "react-router-dom";
import { useStyles } from "./LogoStyles";

const Logo = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <>
      <LogoSVG className={classes.logo} onClick={() => history.push("/")} />
    </>
  );
};

export default Logo;
