import Details from "./CardDetails";
import { useStyles } from "./CardStyles";
const Card = ({ info }) => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.root}>
        <img
          src={info?.image}
          alt="img"
          height="300px"
          width="300px"
          className={classes.photo}
        />
        <div className={classes.details}>
          <Details info={info} />
        </div>
      </div>
    </>
  );
};

export default Card;
