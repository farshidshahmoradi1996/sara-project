import { Grid, Divider, Typography } from "@material-ui/core";
import { ReactComponent as Plane } from "../../assets/WhitePlaneIcon.svg";
import { ReactComponent as Train } from "../../assets/TrainIcon.svg";
import { toMoneyFormat } from "../../utils/common";
const Details = ({ info }) => {
  return (
    <>
      <Grid container style={{ padding: 8 }}>
        <Grid item xs={12}>
          <Typography variant="h4">{info?.title}</Typography>
        </Grid>
        <Grid item xs={5}>
          <Typography> {info?.nights} Nights</Typography>
        </Grid>
        <Grid item xs={1}>
          <Divider orientation="vertical" />
        </Grid>
        <Grid item xs={5}>
          <Typography>{info?.flight_name}</Typography>
        </Grid>
        <Grid item xs={1}>
          <Train height="20px" width="20px" />
        </Grid>
        <Grid item xs={7}>
          <Divider />
        </Grid>
        <Grid item xs={12}>
          <Typography>{toMoneyFormat(info?.prices[0]?.price)} Toman</Typography>
        </Grid>
      </Grid>
    </>
  );
};

export default Details;
