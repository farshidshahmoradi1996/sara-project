import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  root: {
    position: "relative",
    height: "300px",
    display: "flex",
    alignItems: "flex-end",
    color: "white",
    fontWeight: "900",
    cursor: "pointer",
  },
  details: { backgroundColor: "rgba(131, 131, 175,0.5)", width: "300px" },
  photo: {
    position: "absolute",
    top: "0",
    left: "0",
    zIndex: "-1",
  },
});
