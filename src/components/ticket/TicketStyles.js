import { makeStyles } from "@material-ui/core";

const imgUrl = "https://svgur.com/i/U5L.svg";
export const useStyles = makeStyles({
  container: {
    backgroundImage: `url(${imgUrl})`,
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    cursor: "pointer",
  },
  ticket: {
    width: "100%",
    height: "350px",
    padding: "5px",
  },
  ticketContent: {
    marginTop: "30px",
    marginLeft: "15px",
  },
  picture: { borderRadius: 8 },
  button: {
    marginBottom: "15px",
  },
});
