import { ReactComponent as ArrowIcon } from "../../assets/ArrowOutlineIcon.svg";
import { Typography, Grid } from "@material-ui/core";
import { toMoneyFormat } from "../../utils/common";

const AgencyOption = ({ info }) => {
  return (
    <>
      <Grid container>
        <Grid item xs={11}>
          <Typography>{info?.agency_name}</Typography>
        </Grid>
        <Grid item xs={1}>
          <ArrowIcon height="10px" width="10px" />
        </Grid>
        <Grid item xs={12}>
          <Typography align="center">{toMoneyFormat(info?.price)} T</Typography>
        </Grid>
      </Grid>
    </>
  );
};

export default AgencyOption;
