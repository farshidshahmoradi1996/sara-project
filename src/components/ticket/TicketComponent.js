import { useStyles } from "./TicketStyles";
import { Grid, Divider, Button } from "@material-ui/core";
import Details from "./TiketDetails";
import Agency from "./AgancyOptions";
import { useDispatch, useSelector } from "react-redux";
import {
  ADD_ITEM_TO_CART,
  REMOVE_ITEM_FROM_CART,
} from "../../redux/cart/cartActionTypes";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";
const Ticket = ({ info }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const cart = useSelector((state) => state.cart.items);
  const isLoggedIn = useSelector((state) => state.users?.user?.data);
  const addToCart = () => {
    if (!isLoggedIn) {
      toast.error("you should login first .");
      history.push("/login");
      return;
    }
    dispatch({ type: ADD_ITEM_TO_CART, payload: info });
    toast.success(`${info.title} was added to your card successfully.`);
  };
  const removeFromCart = () => {
    dispatch({ type: REMOVE_ITEM_FROM_CART, payload: info.id });
    toast.success(`${info.title} was removed from your card successfully.`);
  };
  const isInCart = cart.find((_i) => _i.id === info.id);
  return (
    <>
      <div className={classes.container}>
        <div className={classes.ticket}>
          <Grid container spacing={1} className={classes.ticketContent}>
            <Grid item xs={3}>
              <img
                className={classes.picture}
                src={info?.image}
                alt="img"
                height="250px"
                width="100%"
              />
            </Grid>

            <Grid item xs={4} style={{ paddingLeft: 12 }}>
              <Details info={info} />
            </Grid>
            <Grid item>
              <Divider orientation="vertical" />
            </Grid>
            <Grid item xs={3}>
              <Agency info={info} />
            </Grid>
          </Grid>
          <Grid container>
            <Grid xs={8} />
            <Grid item xs={4}>
              {isInCart ? (
                <Button
                  variant="contained"
                  color="secondary"
                  className={classes.button}
                  onClick={removeFromCart}
                >
                  Remove From Cart
                </Button>
              ) : (
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  onClick={addToCart}
                >
                  Buy Lower Price
                </Button>
              )}
            </Grid>
          </Grid>
        </div>
      </div>
    </>
  );
};

export default Ticket;
