import { Divider, Grid } from "@material-ui/core";
import Option from "./AgancyOption";

const OptionList = ({ info }) => {
  return (
    <>
      <Grid container spacing={3}>
        {info?.prices.map((item) => (
          <Grid item xs={12} key={item}>
            <Option info={item} />
            <Divider />
          </Grid>
        ))}
      </Grid>
    </>
  );
};

export default OptionList;
