import { ReactComponent as Airplane } from "../../assets/AirplaneIcon.svg";
import { ReactComponent as Hotel } from "../../assets/HotelIcon.svg";
import { Grid, Divider, Typography } from "@material-ui/core";
import { Rating } from "@material-ui/lab";

const Details = ({ info }) => {
  return (
    <>
      <Grid container spacing={1}>
        <Grid item xs={4}>
          <Typography style={{ fontWeight: "bold" }}>{info.title}</Typography>
        </Grid>
        <Grid item xs={4}>
          <Rating disabled value={info?.rank} />
        </Grid>
        <Grid item xs={12}>
          {/* <Typography> lowerPrice </Typography> */}
          <Divider />
        </Grid>
        <Grid item xs={12}>
          <Typography>
            <Hotel height="20px" width="20px" />
            {info.hotel_name}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography>
            services : {info?.hotel_services?.join(" - ")}{" "}
          </Typography>
          <Divider />
        </Grid>
        <Grid item xs={12}>
          <Typography>
            <Airplane height="20px" width="20px" /> {info?.flight_name}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography>
            {" "}
            services :{info?.hotel_services?.join(" - ")}{" "}
          </Typography>
          <Divider />
        </Grid>
      </Grid>
    </>
  );
};

export default Details;
