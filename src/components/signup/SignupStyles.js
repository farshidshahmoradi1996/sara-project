import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  photo: {
    position: "absolute",
    top: "0",
    left: "-42px",
    zIndex: "-1",
    height: "100%",
    width: "100%",
  },
});
