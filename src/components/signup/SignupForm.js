import { Grid, Button, TextField } from "@material-ui/core";
import { NavLink } from "react-router-dom";

const SignUpForm = () => {
  return (
    <>
      <form>
        <Grid container>
          <Grid item xs={11}>
            <TextField label="Email" />
          </Grid>
          <Grid item xs={11}>
            <TextField label="Email" />
          </Grid>
          <Grid item xs={11}>
            <TextField label="Email" />
          </Grid>
          <Grid item xs={11}>
            <Button> Sign Up </Button>
          </Grid>
        </Grid>
      </form>
      <span>
        <p> already have an acount? </p>
        <NavLink to="/login">Login Here</NavLink>
      </span>
    </>
  );
};

export default SignUpForm;
