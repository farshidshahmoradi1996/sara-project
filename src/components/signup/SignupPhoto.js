import { ReactComponent as Photo } from "../../assets/SignUpPhoto.svg";
import { useStyles } from "./SignupStyles";
import Logo from "../logo/Logo";
import { Grid } from "@material-ui/core";

const SignUpPhoto = () => {
  const classes = useStyles();
  return (
    <>
      <Grid container>
        <Grid item xs={4}>
          <Logo />
        </Grid>
        <Grid item xs={12}>
          <Photo className={classes.photo} />
        </Grid>
      </Grid>
    </>
  );
};

export default SignUpPhoto;
