import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  header: {
    position: "absolute",
    top: "0",
    left: "0",
    zIndex: "10",
    marginTop: "10px",
  },
  logo: {
    width: "75%",
    height: "100%",
    marginLeft: "15%",
  },
  ul: {
    display: "flex",
    listStyleType: "none",
    marginLeft: "10px",
    justifyContent: "space-around",
  },
  li: {
    cursor: "pointer",
  },
  currentLi: {
    paddingBottom: "5px",
    borderBottom: "5px solid #2F49D1",
    cursor: "pointer",
  },
  button: {},
});
