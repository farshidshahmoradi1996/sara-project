import { Button, Grid, List, ListItem } from "@material-ui/core";
import { useLocation, useHistory } from "react-router-dom";
import { useStyles } from "./headerStyles";
import Logo from "../logo/Logo";

const Header = ({ isLoggedIn }) => {
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();
  return (
    <>
      <Grid container spacing={3} className={classes.header}>
        <Grid item xs={3}>
          <Logo />
        </Grid>
        <Grid item xs={4}>
          <ul className={classes.ul}>
            <li
              key="1"
              className={
                location.pathname === "/" || location.pathname === "/home"
                  ? classes.currentLi
                  : classes.li
              }
              onClick={() => history.push("/")}
            >
              Home
            </li>
            <li
              key="2"
              className={
                location.pathname === "/joinus" ? classes.currentLi : classes.li
              }
              onClick={() => history.push("/")}
            >
              Join Us
            </li>
            <li
              key="3"
              className={
                location.pathname === "/blogs" ? classes.currentLi : classes.li
              }
              onClick={() => history.push("/")}
            >
              Blogs
            </li>
          </ul>
        </Grid>
        <Grid item xs={2}></Grid>
        <Grid item xs={2}>
          <Button
            color="primary"
            variant="contained"
            fullWidth
            className={classes.button}
            onClick={() =>
              isLoggedIn
                ? history.push("/panel/profile")
                : history.push("/login")
            }
          >
            {isLoggedIn ? "Profile" : "Login"}
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default Header;
