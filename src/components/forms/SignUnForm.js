import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { Link, useHistory } from "react-router-dom";
import { ReactComponent as GoogleIcon } from "../../assets/GoogleIcon.svg";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { userSignUp } from "../../redux/user/userActions";
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="/">
        TripRight
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUpForm() {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const all_users = useSelector((state) => state.users.all_users);
  const loading = useSelector((state) => state.users.user_sign_up_loading);
  const [formState, setFormState] = useState({
    email: "",
    password: "",
    acceptTerms: false,
  });
  const submitForm = (e) => {
    e.preventDefault();
    if (!formState?.email?.trim() || !formState?.password?.trim()) {
      toast.error("Please fill your email and password .");
      return;
    }
    if (!formState?.acceptTerms) {
      toast.error("you need to accept our terms to register account .");
      return;
    }
    if (
      all_users?.find(
        (_i) =>
          _i.email.toLowerCase().trim() ===
          formState?.email?.trim().toLowerCase()
      )
    ) {
      toast.error("You are already registered. Please login to your profile. ");
      return;
    }
    dispatch(userSignUp(formState)).then(() => {
      history.push("/login");
    });
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <br />
      <div className={classes.paper}>
        <Typography component="h1" variant="h3" color="primary" align="center">
          Create Account
        </Typography>
        <br />
        <Button
          variant="contained"
          color="primary"
          startIcon={<GoogleIcon height="25px" width="25px" fullWidth />}
        >
          {"   "}
          Sign in with Google
        </Button>

        <br></br>
        <Typography align="center" color="textSecondary">
          ------------------------------ or ------------------------------
        </Typography>
        <br></br>
        <form className={classes.form} noValidate onSubmit={submitForm}>
          <Typography>Email Address</Typography>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(e) =>
              setFormState({ ...formState, email: e.target.value })
            }
          />
          <br />
          <Typography>Password</Typography>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) =>
              setFormState({ ...formState, password: e.target.value })
            }
          />
          <br />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                value={formState?.acceptTerms}
                onChange={(e) =>
                  setFormState({ ...formState, acceptTerms: e.target.checked })
                }
              />
            }
            label="Agree to Terms and Policy"
          />
          <br />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            disabled={loading}
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}
