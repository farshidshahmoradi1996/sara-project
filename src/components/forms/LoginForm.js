import React, { useReducer, useState } from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { userLogin } from "../../redux/user/userActions";
import { toast } from "react-toastify";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="/">
        TripRight
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function LoginForm() {
  const classes = useStyles();

  const [formState, setFormState] = useState({
    email: "",
    password: "",
    rememberMe: false,
  });
  const dispatch = useDispatch();
  const userState = useSelector((state) => state.users);
  const history = useHistory();

  const submitForm = (e) => {
    e.preventDefault();
    if (!formState?.email?.trim() || !formState?.password?.trim()) {
      toast.error("Please fill your email and password .");
      return;
    }
    dispatch(userLogin(formState));
  };
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <br />
      <div className={classes.paper}>
        <Typography component="h1" variant="h3" color="primary" align="center">
          Get Into Your Account
        </Typography>
        <br />
        <form className={classes.form} noValidate onSubmit={submitForm}>
          <Typography>Email Address</Typography>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            value={formState.email}
            onChange={(e) =>
              setFormState({ ...formState, email: e.target.value })
            }
          />
          <br />
          <Typography>Password</Typography>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={formState.password}
            onChange={(e) =>
              setFormState({ ...formState, password: e.target.value })
            }
          />
          <br />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                value={formState?.rememberMe}
                onChange={(e) =>
                  setFormState({ ...formState, rememberMe: e.target.checked })
                }
              />
            }
            value={formState?.rememberMe}
            label="Remember me"
          />
          <br />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            disabled={userState?.user?.loading}
            className={classes.submit}
          >
            {userState?.user?.loading ? "Please Wait" : "Login"}
          </Button>
          <br />
          <Grid container>
            <Grid item>
              <Link to="/signup">{"Don't have an account? Sign Up"}</Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}
