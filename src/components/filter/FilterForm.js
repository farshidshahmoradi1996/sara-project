import { useEffect, useState } from "react";
import {
  Button,
  TextField,
  Grid,
  MenuItem,
  Paper,
  InputAdornment,
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { useStyles } from "./FilterStyles";
import { ReactComponent as LocationIcon } from "../../assets/LocationIcon.svg";
import { ReactComponent as PersonIcon } from "../../assets/PersonIcon.svg";
import { ReactComponent as ArrowIcon } from "../../assets/ArrowIcon.svg";
import { CITIES } from "./../../assets/mock/toures__mock";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { SET_FILTERS } from "../../redux/filter/filterActionTypes";
import { getTours } from "../../redux/tours/toursActions";

const dFormatter = (d) => {
  if (d > 9) return d;
  return `0${d}`;
};
export const getToday = () => {
  const d = new Date();
  return `${d.getFullYear()}-${dFormatter(d.getMonth() + 1)}-${dFormatter(
    d.getDate()
  )}`;
};
const FilterFrom = () => {
  const history = useHistory();
  const [person, setPerson] = useState(1);
  const [start, setStart] = useState(null);
  const [destination, setDestination] = useState(null);
  const [checkInDate, setCheckInDate] = useState(getToday());
  const filters = useSelector((state) => state.filters);
  const classes = useStyles();
  const handleChange = (event) => {
    setPerson(event.target.value);
  };
  const dispatch = useDispatch();

  const applyFilters = () => {
    //set filters
    const newFilters = {
      ...filters,
      start,
      destination,
    };
    dispatch({
      type: SET_FILTERS,
      payload: newFilters,
    });
    dispatch(getTours(newFilters));
    history.push("/home");
  };

  return (
    <div className={classes.root}>
      <form>
        <Paper className={classes.paper}>
          <Grid
            container
            spacing={3}
            alignItems="center"
            justify="space-around"
          >
            <Grid item md={2} xs={6}>
              <Autocomplete
                id="Cities"
                options={CITIES}
                getOptionLabel={(option) => option.value}
                popupIcon={<LocationIcon height="25px" width="25px" />}
                onChange={(e) => setStart({ value: e.target.innerText })}
                value={start}
                renderInput={(params) => (
                  <TextField {...params} label="Starting" variant="outlined" />
                )}
              />
            </Grid>
            <Grid item md={2} xs={6}>
              <Autocomplete
                id="Cities"
                options={CITIES}
                getOptionLabel={(option) => option.value}
                popupIcon={<LocationIcon height="25px" width="25px" />}
                value={destination}
                onChange={(e) => setDestination({ value: e.target.innerText })}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Destination"
                    variant="outlined"
                  />
                )}
              />
            </Grid>
            <Grid item md={2} xs={6}>
              <TextField
                id="date"
                label="Check in"
                type="date"
                fullWidth
                onChange={(e) => setCheckInDate(e.target.value)}
                value={checkInDate}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>

            <Grid item md={3} xs={6}>
              <TextField
                id="person"
                select
                label="Person"
                value={person}
                onChange={handleChange}
                fullWidth
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <PersonIcon height="25px" width="25px" />
                    </InputAdornment>
                  ),
                }}
              >
                {[
                  { value: 1, label: "1" },
                  { value: 2, label: "2" },
                  { value: 3, label: "3" },
                ].map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={1}>
              <Button
                color="primary"
                onClick={applyFilters}
                variant="contained"
              >
                <ArrowIcon height="25px" width="25px" />
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </form>
    </div>
  );
};

export default FilterFrom;
