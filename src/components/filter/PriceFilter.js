import { useState } from "react";
import { Paper, Typography, Slider, Grid, Button } from "@material-ui/core";
import { useStyles } from "./FilterStyles";
import { useDispatch, useSelector } from "react-redux";
import { toMoneyFormat } from "../../utils/common";
import { SET_FILTERS } from "../../redux/filter/filterActionTypes";
import { getTours } from "../../redux/tours/toursActions";
function valueText(value) {
  return `${value}$`;
}
const PriceFilter = () => {
  const filters = useSelector((state) => state.filters);
  const [value, setValue] = useState([
    filters?.minPrice || 0,
    filters?.maxPrice || 40000000,
  ]);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const dispatch = useDispatch();
  const classes = useStyles();
  const apply = (clear = false) => {
    //set filters
    const newFilters = {
      ...filters,
      minPrice: clear ? null : value[0],
      maxPrice: clear ? null : value[1],
    };

    dispatch({
      type: SET_FILTERS,
      payload: newFilters,
    });
    dispatch(getTours(newFilters));
  };

  return (
    <>
      <Paper className={classes.pricePaper}>
        <Grid container>
          <Grid item xs={12}>
            <Typography> Price:</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography>{`${toMoneyFormat(value[0])} T`}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography>{`${toMoneyFormat(value[1])} T`}</Typography>
          </Grid>
          <Grid item xs={12}>
            <Slider
              value={value}
              onChange={handleChange}
              valueLabelDisplay="off"
              getAriaValueText={valueText}
              max={40000000}
              step={100000}
            />
          </Grid>
          <Button onClick={() => apply(false)} color={"secondary"}>
            Apply Filter
          </Button>
          {filters?.maxPrice && (
            <Button onClick={() => apply(true)} color={"primary"}>
              Clear Filter
            </Button>
          )}
        </Grid>
      </Paper>
    </>
  );
};

export default PriceFilter;
