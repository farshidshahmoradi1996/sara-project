import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  root: {
    position: "absolute",
    top: "280px",
    left: "60px",
    right: "400px",
    zIndex: "2",
  },
  paper: {
    padding: "25px",
  },
  relative: {
    position: "relative",
    top: "100px",
  },
  pricePaper: {
    width: "350px",
    margin: "15px",
    padding: "25px",
  },
});
