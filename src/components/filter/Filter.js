import { useState } from "react";
import {
  Button,
  TextField,
  Grid,
  MenuItem,
  Paper,
  InputAdornment,
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { useStyles } from "./FilterStyles";
import { ReactComponent as LocationIcon } from "../../assets/LocationIcon.svg";
import { ReactComponent as PersonIcon } from "../../assets/PersonIcon.svg";
import { ReactComponent as ArrowIcon } from "../../assets/ArrowIcon.svg";
import { CITIES } from "../../assets/mock/toures__mock";
import { useDispatch, useSelector } from "react-redux";
import { SET_FILTERS } from "../../redux/filter/filterActionTypes";
import { getTours } from "../../redux/tours/toursActions";
import { getToday } from "./FilterForm";
const FilterFrom = () => {
  const [person, setPerson] = useState(1);
  const filters = useSelector((state) => state.filters);

  const [start, setStart] = useState(
    filters?.start?.value ? { value: filters?.start?.value } : null
  );
  const [destination, setDestination] = useState(
    filters?.destination?.value ? { value: filters?.destination?.value } : null
  );
  const [checkInDate, setCheckInDate] = useState(getToday());
  const handleChange = (event) => {
    setPerson(event.target.value);
  };
  const dispatch = useDispatch();

  const submit = () => {
    //set filters
    const newFilters = {
      ...filters,
      start,
      destination,
    };
    dispatch({
      type: SET_FILTERS,
      payload: newFilters,
    });
    dispatch(getTours(newFilters));
  };

  const classes = useStyles();
  return (
    <div className={classes.relative}>
      <form>
        <Paper className={classes.paper}>
          <Grid
            container
            spacing={3}
            alignItems="center"
            justify="space-around"
          >
            <Grid item md={2} xs={6}>
              <Autocomplete
                id="Starting"
                value={start}
                onChange={(e) => setStart({ value: e.target.innerText })}
                options={CITIES}
                getOptionLabel={(option) => option.value}
                popupIcon={<LocationIcon height="25px" width="25px" />}
                renderInput={(params) => (
                  <TextField {...params} label="Starting" variant="outlined" />
                )}
              />
            </Grid>
            <Grid item md={2} xs={6}>
              <Autocomplete
                id="Destination"
                options={CITIES}
                value={destination}
                onChange={(e) => setDestination({ value: e.target.innerText })}
                getOptionLabel={(option) => option.value}
                popupIcon={<LocationIcon height="25px" width="25px" />}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Destination"
                    variant="outlined"
                  />
                )}
              />
            </Grid>

            <Grid item md={2} xs={6}>
              <TextField
                id="date"
                label="Check in"
                type="date"
                fullWidth
                value={checkInDate}
                onChange={(e) => setCheckInDate(e.target.value)}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid item md={3} xs={6}>
              <TextField
                id="person"
                select
                label="Person"
                value={person}
                onChange={handleChange}
                fullWidth
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <PersonIcon height="25px" width="25px" />
                    </InputAdornment>
                  ),
                }}
              >
                {[
                  { value: 1, label: "1" },
                  { value: 2, label: "2" },
                  { value: 3, label: "3" },
                ].map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={1}>
              <Button onClick={submit} color="primary" variant="contained">
                <ArrowIcon height="25px" width="25px" />
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </form>
    </div>
  );
};

export default FilterFrom;
