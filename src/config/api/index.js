import axios from "axios";
const __DEV__ = process.env.NODE_ENV !== "production";
const baseURL = __DEV__
  ? "/api/v1/Enter-account"
  : "https://travelright.herokuapp.com/api/v1/Enter-account";

const instance = axios.create({
  baseURL,
  timeout: 10000,
});

export default instance;
