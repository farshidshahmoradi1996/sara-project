import Routes from "./Routes";
import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";
import { useEffect } from "react";
import { useSelector } from "react-redux";

const App = () => {
  const cart = useSelector((state) => state.cart.items);
  const all_users = useSelector((state) => state.users.all_users);
  const userData = useSelector((state) => state.users.user.data);
  const state = useSelector((state) => state);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  useEffect(() => {
    localStorage.setItem("all_users", JSON.stringify(all_users));
  }, [all_users]);

  useEffect(() => {
    localStorage.setItem("session", JSON.stringify(userData));
  }, [userData]);

  console.log("state", state);

  return (
    <>
      <Routes isLoggedIn={true} />
      <ToastContainer />
    </>
  );
};

export default App;
